import React, { Component } from "react";
import {
    View,
    Text,
    TouchableOpacity,
} from "react-native";
import {allStyle} from "../stylesheet";
import {connect} from 'react-redux';

class MainScreen extends Component {

    constructor(props){
        super(props);
        if(!this.props.token.id_token){
            alert('Login Terlebih Dahulu');
            this.props.navigation.navigate('LoginScreen')
        }

    }



    render() {
        return (
            <View style={allStyle.container}>
                <TouchableOpacity
                    style={allStyle.menuButton}
                    onPress={()=>{this.props.navigation.navigate('TrackingScreen')}}
                >
                    <Text>LACAK</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={allStyle.menuButton}
                    onPress={()=>{alert(this.props.token.id_token)}}
                >
                    <Text>PROFIL</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        ...state,
        id_token: state.id_token
    };
};

export default connect(mapStateToProps)(MainScreen);
