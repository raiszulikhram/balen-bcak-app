import React, { Component } from "react";
import {
    View,
    Text,
    TextInput,
    TouchableOpacity
} from "react-native";
import {allStyle} from '../stylesheet'
import {connect} from 'react-redux';
import axios from 'axios';
import {setToken} from "../actions/index";


class LoginScreen extends Component {

    constructor(props){
      super(props);
      this.state={
        username:'',
        password:''
      };
    }

    login=()=>{

      let responseBody = {
        username:this.state.username,
        password:this.state.password
      };

      axios.post('http://157.230.36.131:8080/api/authenticate',responseBody)
          .then ((response)=>{
              let id_token = response.data.id_token;
              if(id_token || this.props.token.id_token){
                  this.props.dispatch(setToken(id_token));
                  this.props.navigation.navigate('MainScreen');
              }
          })
          .catch((error)=>{
              alert(error)
          });

      this.setState({});

    };


    render() {
        return (
            <View style={allStyle.container}>
                <TextInput
                    value={this.state.username}
                    name="email"
                    autoCapitalize = "none"
                    placeholder="EMAIL"
                    placeholderTextColor="#ffffff"
                    onChangeText={(text)=>this.setState({username:text})}
                    style ={allStyle.input}
                />
                <TextInput
                    value={this.state.password}
                    name="password"
                    autoCapitalize = "none"
                    placeholder="PASSWORD"
                    placeholderTextColor="#ffffff"
                    onChangeText={(text)=>this.setState({password:text})}
                    style ={allStyle.input}
                />
                <TouchableOpacity
                    onPress={this.login}
                    style = {[allStyle.button,{backgroundColor: '#FAFF00'}]}
                >
                    <Text style={{color: '#0082AB'}}> MASUK </Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style = {[allStyle.button,{backgroundColor:'#03A9DD'}]}
                    onPress={()=>{this.props.navigation.navigate('SignUpScreen')}}
                >
                    <Text style={{color: '#ffffff'}}> DAFTAR </Text>
                </TouchableOpacity>
            </View>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        ...state,
        id_token: state.id_token
    };
};

export default connect(mapStateToProps)(LoginScreen);
