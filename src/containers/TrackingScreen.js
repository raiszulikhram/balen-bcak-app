import React,{Component} from 'react';
import {
    View,
    StyleSheet,
    TouchableOpacity,
    Text,
} from 'react-native';
import MapView, {Marker} from "react-native-maps";
import {connect} from 'react-redux';

class TrackingScreen extends Component {

    constructor(props){
        super(props);

        this.state={
            latitude:0,
            longitude:0,
            latitudeDelta:0.1,
            longitudeDelta:0.1,
        };

        this.getLocation();

    }

    getLocation = ()=>{
        navigator.geolocation.getCurrentPosition(
            position => {
                this.setState({latitude: position.coords.latitude});
                this.setState({longitude: position.coords.longitude})
            },
            error =>{
                alert(error.message)
            }
        );
    };

    zoomOut = ()=>{
        let latDelta = this.state.latitudeDelta*1.1;
        let longDelta = this.state.longitudeDelta*1.1;
        this.setState({latitudeDelta:latDelta,longitudeDelta:longDelta})
    };

    zoomIn = ()=>{
        let latDelta = this.state.latitudeDelta*0.9;
        let longDelta = this.state.longitudeDelta*0.9;
        this.setState({latitudeDelta:latDelta,longitudeDelta:longDelta})
    };

    connect =()=> {
        // this.props.stompContext.newStompClient("http://157.230.36.131:8080/websocket/tracker").subscribe('/', (message) => {console.log(message.body)})

    };

    componentDidMount() {

    }

    componentWillUnmount() {
        this.props.stompContext.removeStompClient()
    }

    render(){

        return(
            <View style={styles.container}>

                <MapView
                    style={styles.map}
                    region={{
                        latitude: this.state.latitude,
                        longitude: this.state.longitude,
                        latitudeDelta:this.state.latitudeDelta,
                        longitudeDelta:this.state.longitudeDelta
                    }}
                >

                    <Marker
                        coordinate={{latitude:this.state.latitude,
                        longitude:this.state.longitude}}
                    >

                    </Marker>

                </MapView>

                <View style={[styles.container,{justifyContent:'flex-end',paddingBottom:50}]}>

                    <View style ={{flexDirection:'row'}} >

                        <TouchableOpacity
                            style={styles.zoomButton}
                            onPress={this.zoomOut}>
                            <Text>-</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={styles.zoomButton}
                            onPress={this.zoomIn}>
                            <Text>+</Text>
                        </TouchableOpacity>

                    </View>

                    <TouchableOpacity
                        style={styles.button}
                        onPress={this.getLocation}>
                        <Text>current location</Text>
                    </TouchableOpacity>

                    {/*{this.connect()};*/}
                </View>

            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        ...state,
        id_token: state.id_token
    };
};

export default connect(mapStateToProps)(TrackingScreen);

const styles = StyleSheet.create({
    container:{
        position:"absolute",
        top:0,
        bottom:0,
        left:0,
        right:0,
        justifyContent:"flex-end",
        alignItems:"center"
    },
    map:{
        position:"absolute",
        top:0,
        bottom:0,
        left:0,
        right:0
    },
    button:{
        padding:15,
        width:300,
        margin:10,
        height:75,
        alignItems:'center',
        borderRadius:10,
        backgroundColor:'#00A1D5',
        justifyContent: 'center',
        elevation:10
    },
    zoomButton:{
        padding:15,
        width:140,
        margin:10,
        height:75,
        alignItems:'center',
        borderRadius:10,
        backgroundColor:'#00A1D5',
        justifyContent: 'center',
        elevation:10
    }
});
