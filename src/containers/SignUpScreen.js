import React, { Component } from "react";
import {
    View,
    Text,
    TextInput,
    TouchableOpacity
} from "react-native";
import {allStyle} from "../stylesheet";
import {connect} from 'react-redux';

class SignUpScreen extends Component {

    constructor(props){
        super(props);
        this.state={
            firstName:'',
            lastName:'',
            email:'',
            id:0,
            pin:'',
            pin_confirmation:''
        };
    }

    signUp=() =>{
        let responseBody ={
            createdBy: "string",
            createdDate: "2020-02-05T03:14:48.508Z",
            email: "string",
            firstName: "string",
            id: 0,
            lastName: "string",
            login: "string",
            password: "string"
        }
    };

    render() {
        return (
            <View style={allStyle.container}>
                <TextInput
                    value={this.state.firstName}
                    placeholder="NAMA DEPAN"
                    autoCapitalize="characters"
                    placeholderTextColor="#ffffff"
                    onChangeText={(text)=>this.setState({firstName:text})}
                    style ={allStyle.input}
                />
                <TextInput
                    value={this.state.lastName}
                    placeholder="NAMA BELAKANG"
                    autoCapitalize="characters"
                    placeholderTextColor="#ffffff"
                    onChangeText={(text)=>this.setState({lastName:text})}
                    style ={allStyle.input}
                />
                <TextInput
                    value={this.state.email}
                    placeholder="EMAIL"
                    autoCapitalize="characters"
                    placeholderTextColor="#ffffff"
                    onChangeText={(text)=>this.setState({email:text})}
                    style ={allStyle.input}
                />
                <TextInput
                    value={this.state.device_id}
                    autoCapitalize = "none"
                    placeholder="NOMOR ALAT"
                    placeholderTextColor="#ffffff"
                    onChangeText={(text)=>this.setState({device_id:text})}
                    style ={allStyle.input}
                />
                <TextInput
                    value={this.state.pin}
                    autoCapitalize = "none"
                    placeholder="PIN 6 ANGKA"
                    placeholderTextColor="#ffffff"
                    onChangeText={(text)=>this.setState({pin:text})}
                    style ={allStyle.input}
                />
                <TextInput
                    value={this.state.pin_confirmation}
                    autoCapitalize = "none"
                    placeholder="ULAGI PIN 6 ANGKA"
                    placeholderTextColor="#ffffff"
                    onChangeText={(text)=>this.setState({pin_confirmation:text})}
                    style ={allStyle.input}
                />
                <TouchableOpacity
                    onPress={this.signUp}
                    style = {[allStyle.button,{backgroundColor: '#FAFF00'}]}
                >
                    <Text style={{color: '#0082AB'}}>DAFTAR</Text>
                </TouchableOpacity>
            </View>
        );
    }
}
export default connect()(SignUpScreen);
