import * as ACTION_TYPES from './actionTypes'

export const setToken = (text)=> ({
    type: ACTION_TYPES.SET_ID_TOKEN,
    id_token:text
});
