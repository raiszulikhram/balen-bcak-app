import React, { Component } from "react";

import LoginScreen from "./containers/LoginScreen";
import SignUpScreen from "./containers/SignUpScreen";
import MainScreen from "./containers/MainScreen";
import TrackingScreen from "./containers/TrackingScreen";
import ProfileScreen from "./containers/ProfileScreen";

import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

const RootStack = createStackNavigator({
    MainScreen:{
        screen:MainScreen,
        navigationOptions:{
            headerTitle:'balen',
            headerTitleAlign:'center'
        }
    },
    LoginScreen: {
        screen:LoginScreen,
        navigationOptions:{
            header:()=>false
        }
    },
    SignUpScreen:{
        screen:SignUpScreen,
        navigationOptions:{
            headerTitle:'balen sign up',
            headerTitleAlign:'center'
        }
    },
    ProfileScreen:{
        screen:ProfileScreen,
        navigationOptions:{
            headerTitle:'profile',
            headerTitleAlign:'center'
        }
    },
    TrackingScreen:{
        screen:TrackingScreen,
        navigationOptions:{
            headerTitle:'lacak',
            headerTitleAlign:'center'
        }
    },
});

const AppContainer = createAppContainer(RootStack);


class BcakApp extends Component {

    render() {
        return (
            <AppContainer/>
        );
    }
}
export default BcakApp;
