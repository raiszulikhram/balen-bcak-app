import * as ACTION_TYPES from '../actions/actionTypes'

const login = (state={},action)=>{
    switch (action.type){
        case ACTION_TYPES.LOGIN_ATTEMPTED :
            return {
                ...state,
                email:action.payload.email,
                password:action.payload.password,
            };
        case ACTION_TYPES.LOGIN_SUCCESS :
            return {
                ...state,
                id_token: action.id_token
            };
        case ACTION_TYPES.LOGIN_FAILED :
            return {
                ...state,
                error: action.error
            };
        default:return state;
    }

};

export default login;
