import * as ACTION_TYPES from '../actions/actionTypes'

const initialState ={
    id_token:''
};

const token = (state=initialState,action) =>{
    switch (action.type){
        case ACTION_TYPES.SET_ID_TOKEN :
            return {
                ...state,
                id_token:action.id_token
            };
        default:
            return state;
    }
};

export default token;
