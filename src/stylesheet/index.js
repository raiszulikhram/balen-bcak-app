import {StyleSheet} from "react-native";

export const allStyle = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#00B3EC',
        justifyContent: 'center',
        alignItems:'center',
        paddingTop: 23,
    },
    input: {
        backgroundColor:'#00B0E8',
        margin: 10,
        height: 50,
        borderColor: '#ffffff',
        borderBottomWidth: 5,
        width:300
    },
    button:{
        padding:15,
        width:300,
        margin:10,
        height:50,
        alignItems:'center',
        borderRadius:10,
        justifyContent: 'center'
    },
    menuButton:{
        padding:15,
        width:300,
        margin:10,
        height:75,
        alignItems:'center',
        borderRadius:10,
        backgroundColor:'#00A1D5',
        justifyContent: 'center',
        elevation:10
    }
});
