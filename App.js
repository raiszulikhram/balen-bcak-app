import React from 'react';
import BcakApp from './src/BcakApp';
import store from './src/store/index';
import {Provider} from 'react-redux'

export default function App() {
  return (
      <Provider store={store}>
        <BcakApp/>
      </Provider>
  );
}
